# Curso de desarrollo web Full Stack

El objetivo de este curso es adquirir las competencias necesarias para desarrollar paginas web de A a Z que respondan a la necesidad del cliente.

# Temario

## Interfaz de usuario
### [Estructura de una pagina (HTML)](/seccion/1_html.md)
### Estilos (CSS)
### Comportamiento (Javascript)
### Proyecto

## Procesamiento de la información
### [Tratamiento (PHP)](/seccion/data_processing_php.md)
### Persistencia (Base de datos)
### Proyecto

### Proyecto Completo (Agenda)