# Estructura de una pagina HTML

En esta sección encontraremos los conceptos básicos a aprender sobre HTML.

## Qué es HTML?
### Como funciona? Como se ejecuta?
### Con que herramientas se puede maquetar en HTML?
### Que significa debuggear? Que herramienta se puede usar para debugger?

## Estructura HTML
### Que es un elemento y de que esta compuesto?
### Que es un atributo y de que esta compuesto?
### Que es un documento HTML? cual es su estructura básica? que etiquetas son las mas importantes?

### Elementos


En HTML existen muchos tipos de elementos/etiquetas que nos permites desarrollar nuestra pagina web: **texto, lista, formulario, audio, video, marco/iframe, etc**.

Puedes encontrar una lista completa de estos elementos en la pagina de [Mozilla](https://developer.mozilla.org/es/docs/Web/HTML/Block-level_elements).

### Que tipos de elementos de texto son los mas utilizados? cual es su función?
### Que tipo de elementos de lista que existen? cual es su función?

### Caso de uso (Pagina de contacto - despacho de abogados)

Fuimos contratados para realizar la pagina de contacto de un despacho de abogados en Ucrania. 
En principio, tu objetivo es desarrollar la estructura HTML de esta pagina (sin estilos css) partiendo de la siguiente maqueta. 


![1_html.md](/seccion/image/html_1.PNG)

1. Enlaces a paginas externas
2. Mapa de google mostrando la ubicación del despacho de abogados 

