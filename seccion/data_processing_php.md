# Tratamiento de la información PHP

En esta sección encontraremos los conceptos básicos a aprender sobre PHP.

## Pre-requisitos
1. Tener instalado [Visual Studio Code](https://code.visualstudio.com/)

## Estructura de un archivo PHP
Todo código PHP se programa dentro de un bloque de código principal PHP como se muestra debajo.

```
<?php 
    /// nuestro código PHP
?>
```

## Imprimir un mensaje en pantalla
El uso de la función ``echo`` permite la impresión de mensajes en pantalla.

```
<?php 
    echo "este es un mensaje";
?>
```

> Nota: el final de una sentencia en PHP se marca con un `punto y coma`. Si este no se agrega, entonces se produce un `error`.

## Tipos de datos y variables
En PHP existen ``dos tipos de datos`` distintos, los ``escalares`` y los ``compuestos``. Los datos ``escalares`` son aquellos que contienen ``un valor  en un momento dado``: son tipos escalares los valores numéricos, alfanuméricos, punto flotante y booleanos. Mientras que los datos ``compuestos`` contienen varios valores en un mismo momento: son tipos compuestos los objetos y los arreglos/listas.

### Tipos de datos escalares
* `Int` o número entero con signo
    * conjunto ℤ = {..., -2, -1, 0, 1, 2, ...}
* `Float` o número fraccionario (coma flotante) con signo
    * el separador de decimales en PHP se representa con el punto (notación anglosajona)
    * Ej: 1024.36 (mil veinticuatro coma treinta y seis)
* `String` o dato alfanumérico
    * Ej: "Hola PHP"
* `Booleano` de valor `true` o `false` (verdadero o falso)

### Variables
Una variable en informática es representa un espacio en el sistema de almacenamiento de información y un símbolo que asocia un nombre (identificador) a un valor en dicho sistema de almacenado.
La información guardada por una variable es `volatil` y `efímera`. Volatil porque el valor de esta puede cambiar durante toda su tiempo de vida. Efímera porque el tiempo de vida de la variable esta limitado al tiempo de ejecución del programa en donde esta existe. Símbolo

#### Estructura de una variable en PHP
Una variable en PHP comienza con el `símbolo $` seguida de su nombre de variable: ``$name``. 

> Nota: el símbolo `$` es obligatorio para usar una variable. Si este no se usa, entonces PHP arrojara un error al momento de 

![php_1.png](/seccion/image/php_1.png)



## Referencias

https://programador-php.net/tipos-de-datos-en-php/+



